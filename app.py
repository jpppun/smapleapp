from flask import Flask,Blueprint
from flask_restful import Api, Resource, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import *
import random



class SampleAPI(Resource):
    def get(self):
        return ':) {}'.format(random.randint(0,10))

app= Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']=SQLALCHEMY_DATABASE_URI
db=SQLAlchemy(app)





if __name__ =="__main__":
    from api.api.blueprint import bp
    app.register_blueprint(bp,url_prefix="/api")
    app.run("0.0.0.0",port=5001,debug=True)