dir=$(dirname $(test -L "$BASH_SOURCE" && readlink -f "$BASH_SOURCE" || echo "$BASH_SOURCE"))
eval $(minikube docker-env)
kubectl delete -f $dir/../k8s/api.yaml
docker build -t api $dir/..
kubectl apply -f $dir/../k8s/api.yaml