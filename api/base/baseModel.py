from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column
from sqlalchemy import Integer,String
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.declarative import declarative_base
from app import db
import json


class Base(db.Model):
    __abstract__ = True
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()
    @classmethod
    def __build_query(cls,filters=[]):
        q= db.session.query(cls)
        for f in filters:
            q=q.filter(f)
        return q



    @classmethod
    def get_all(cls,**kargs):
        return cls.__build_query(**kargs).all()
    @classmethod
    def first_or_none(cls,**kargs):
        return cls.__build_query(**kargs).first()
    @classmethod
    def get_one(cls,**kargs):
        return cls.__build_query(**kargs).one_or_none()
    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}



def idCoulmn(length=128,**kargs):
    return Column(String(length=length),**kargs,primary_key=True) 
def stringCoulmn(length=128,**kargs):
   return Column(String(length=length),**kargs) 
