from flask_restful import Resource,reqparse,request
from app import db
import json
import inspect
from api.model.users import User

class BaseResource(Resource):
    USER_TOKEN_HEADER ="APP_TOKEN_HEADER"
    def post(self, *args, **kwargs):
        return self.r("POST", *args, **kwargs)
    
    def get(self, *args, **kwargs):
        return self.r("GET", *args, **kwargs)
        
    def delete(self, *args, **kwargs):
        return self.r("DELETE", *args, **kwargs)
    def put(self, *args, **kwargs):
        return self.r("PUT", *args, **kwargs)

    def r(self,method, *args, **kwargs):
        parser=reqparse.RequestParser()
        if not hasattr(self,method[0].lower()):
            raise Exception("Not implemented")
        handle=self.__getattribute__(method[0].lower())
        if "user" in inspect.signature(handle).parameters: #function require user object
            if self.USER_TOKEN_HEADER in request.headers:
                token_header = request.headers[self.USER_TOKEN_HEADER]
            else:
                token_header = None
            print (token_header)
            if not token_header:
                raise Exception("no header presented")
            user= User.get_by_token(token_header)
            if not user:
                raise Exception("user not found")
            return handle(user=user)
        else:
            return handle()
