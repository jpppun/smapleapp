from flask import Blueprint
from flask_restful import Api
from api.api.user import UserHandler


bp=Blueprint('api',__name__)
api=Api(bp)
#api.add_resource(SampleAPI,'/')
api.add_resource(UserHandler,'/users')