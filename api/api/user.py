from api.base.baseResource import BaseResource
from api.model.users import User

class UserHandler(BaseResource):
    def g(self,user):
        return [user.to_json() for user in User.get_all()]
    