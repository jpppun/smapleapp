from api.base.baseModel import Base,idCoulmn,stringCoulmn
from api.model.token import Token

class User(Base):
    id=idCoulmn()
    name=stringCoulmn(length=128)

    @classmethod
    def get_by_id(cls,id):
        return cls.get_one(filters=[User.id==id])

    @classmethod
    def get_by_token(cls,token):
        token=Token.get_by_token(token)
        if not token or token.obj_type !="user":
            return None
        return User.get_by_id(token.obj_id)
    
        