from api.base.baseModel import *


class Token(Base):
    obj_type=stringCoulmn(primary_key=True)
    obj_id=stringCoulmn(primary_key=True)
    token=stringCoulmn(index=True)

    @classmethod
    def get_by_token(cls,token):
        return cls.get_one(filters=[Token.token==token])
    
    

    